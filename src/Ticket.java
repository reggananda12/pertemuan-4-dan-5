public class Ticket {
    
    private String asalPenumpang;
    private String tujuanPenumpang;
    private String namaPenumpang;

    public Ticket(String namaPenumpang){
        this.namaPenumpang = namaPenumpang;
    }

    public Ticket(String asalPenumpang, String tujuanPenumpang, String namaPenumpang){
        this.asalPenumpang = asalPenumpang;
        this.tujuanPenumpang = tujuanPenumpang;
        this.namaPenumpang = namaPenumpang;
    }
    
    public String getAsal(){
        return asalPenumpang;
    }

    public String getTujuan(){
        return tujuanPenumpang;
    }

    public String getNamaPenumpang(){
        return namaPenumpang;
    }

}