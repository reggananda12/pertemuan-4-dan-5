public class Kereta {

    public String namaKereta;
    private int jumlahTiket;
    private int banyakpenumpang;
    private Ticket[] banyakTiket;

    public Kereta() {
        this.namaKereta = "Komuter";
        this.jumlahTiket = 1000;
        banyakTiket = new Ticket[jumlahTiket];
    }

    public Kereta(String namaKereta, int jumlahTiket) {
        this.namaKereta = namaKereta;
        this.jumlahTiket = jumlahTiket;
        banyakTiket = new Ticket[this.jumlahTiket];
    }
    
    public void tambahTiket(String namaPenumpang) {
        if(banyakpenumpang < jumlahTiket){
            Ticket ekonomi = new Ticket(namaPenumpang);
            banyakTiket[banyakpenumpang] = ekonomi;
            banyakpenumpang++;
            System.out.println("========================================================");
            if(jumlahTiket - banyakpenumpang >= 30){
                System.out.println("Tiket Berhasil Dipesan");
            }
            else if(jumlahTiket - banyakpenumpang < 30){
                System.out.println("Tiket Berhasil Dipesan, Sisa Tiket Tersedia "+ (jumlahTiket - banyakpenumpang));
            }     
        }
        else{
            System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
        }
    }

    public void tambahTiket(String namaPenumpang, String asalPenumpang, String tujuanPenumpang) {
        if(banyakpenumpang < jumlahTiket){
            Ticket ekonomi = new Ticket(namaPenumpang, asalPenumpang, tujuanPenumpang);
            banyakTiket[banyakpenumpang] = ekonomi;
            banyakpenumpang++;
            System.out.println("========================================================");
            if(jumlahTiket - banyakpenumpang >= 30){
                System.out.println("Tiket Berhasil Dipesan");  
            }
            else if(jumlahTiket - banyakpenumpang < 30){
                System.out.println("Tiket Berhasil Dipesan, Sisa Tiket Tersedia "+ (jumlahTiket - banyakpenumpang));
            }     
        }
        else{
            System.out.println("========================================================");
            System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
        }
    }
    public void tampilkanTiket() {
        System.out.println("========================================================");
                System.out.println("Tiket untuk kereta api " + namaKereta + ":");
                System.out.println("-----------------------------");
        for(int aa = 0; aa < banyakpenumpang; aa++){
            if(banyakTiket[aa].getAsal() != null){
                System.out.println("Nama : "+ banyakTiket[aa].getNamaPenumpang());
                System.out.println("Asal : "+ banyakTiket[aa].getAsal());
                System.out.println("Tujuan : "+ banyakTiket[aa].getTujuan());
                System.out.println("-----------------------------");
            }
            else{
                System.out.println("Nama : "+ banyakTiket[aa].getNamaPenumpang());
            }
        }
    }
}